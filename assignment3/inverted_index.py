import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
	# key: document identifier
	# value: document contents
	key = record[0]
	value = record[1]
	words = value.split()
	for w in words:
		if not mr.intermediate.has_key(w):
			mr.emit_intermediate(w, key)
		elif key not in mr.intermediate[w]:
			mr.emit_intermediate(w, key)

def reducer(key, list_of_documents):
    # key: word
    # list_of_documents: list of documents in which keyword occurs
    mr.emit((key, list_of_documents))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
