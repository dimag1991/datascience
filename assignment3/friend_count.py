import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    person = record[0]
    mr.emit_intermediate(person, 1)

def reducer(key, list_of_counts):
    total_friend_num = 0
    for v in list_of_counts:
      total_friend_num += v
    mr.emit((key, total_friend_num))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
