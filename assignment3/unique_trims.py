import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # dna_trim: truncated DNA sequence used as intermediate key as well
    dna_trim = record[1][:-10]
    mr.emit_intermediate(dna_trim, 1)

def reducer(key, list_of_occurrences):
    # key: unique dna_trim
    mr.emit(key)

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
