import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
	# key: order_id
	# value: database record
	key = record[1]
	value = record
	mr.emit_intermediate(key, value)

def reducer(key, list_of_records):
    # key: order_id
    # value: list of records with "key" order_id
	list_of_join_records = []
	count = 0
	for record in list_of_records:
		count += 1
		if record[0] == 'order':
			for rec in list_of_records[count:]:
				if rec[0] == 'line_item':
					list_of_join_records.append(record + rec);
		else:
			for rec in list_of_records[count:]:
				if rec[0] == 'order':
					list_of_join_records.append(rec + record);
	for join_record in list_of_join_records:
		mr.emit(join_record)

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
