import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
	person = record[0]
	friend = record[1]
	mr.emit_intermediate((person, friend), 1)
	mr.emit_intermediate((friend, person), -1)

def reducer(key, value):
	sum_val = sum(value)
	p1 = key[0]
	p2 = key[1]
	if sum_val == -1:
		mr.emit((p1, p2))
		mr.emit((p2, p1))
		

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
