import MapReduce
import sys

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
	matrix_id = record[0]
	if matrix_id == 'a':
		i = record[1]
		for k in range(5):
			mr.emit_intermediate((i, k), record)
	else:
		k = record[2]
		for i in range(5):
			mr.emit_intermediate((i, k), record)

def reducer(key, list_of_values):
	product_element = 0
	vecA = {}
	vecB = {}
	for val in list_of_values:
		if val[0] == 'a':
			vecA[int(val[2])] = int(val[3])
		else:
			vecB[int(val[1])] = int(val[3])
	for a in vecA:
		if vecB.has_key(a):
			product_element += vecA[a]*vecB[a]
	mr.emit((key[0], key[1], product_element))

# Do not modify below this line
# =============================
if __name__ == '__main__':
  inputdata = open(sys.argv[1])
  mr.execute(inputdata, mapper, reducer)
