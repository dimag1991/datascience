import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

class SpamFilterTest {
	public static void test(String train, String test) throws IOException {
		HashMap<String, List<String>> data = new HashMap<String, List<String>>();
        BufferedReader input =  new BufferedReader(new FileReader(train));
        try {
            String line = null;
            while (( line = input.readLine()) != null) {
				String[] tokens = line.split(" ");
				String classLabel = tokens[1];
                if(data.containsKey(classLabel)) {
					data.get(classLabel).add(line);
				} else {
					data.put(classLabel, new ArrayList<String>());
					data.get(classLabel).add(line);
				}
            }
        }
        finally {
            input.close();
        } 
		for(int i = 1; i < (1 << 5); i *= 2) {
			int scale = i;
			SpamFilter sf = new SpamFilter(data, scale);
			input =  new BufferedReader(new FileReader(test));
			int testExNum = 0;
			int correctNum = 0;
			try {
				String line = null;
				while (( line = input.readLine()) != null) {
					String[] email = line.split(" ");
					testExNum++;
					String label = sf.getClassLabel(email);
					if(label.equals(email[1])) {
						correctNum++;
					}
				}
			}
			finally {
				input.close();
			}
			StdOut.println("Smoothing param: " + scale + " Accuracy: " + (correctNum + 0.0)/testExNum);
		}
	}
	
	public static void streamTest(String train, String test) throws IOException {
		for(int i = 1; i < (1 << 5); i *= 2) {
			int scale = i;
			SpamFilter sf = new SpamFilter(train, scale);
			BufferedReader input =  new BufferedReader(new FileReader(test));
			int testExNum = 0;
			int correctNum = 0;
			try {
				String line = null;
				while (( line = input.readLine()) != null) {
					String[] email = line.split(" ");
					testExNum++;
					String label = sf.getClassLabel(email);
					if(label.equals(email[1])) {
						correctNum++;
					}
				}
			}
			finally {
				input.close();
			}
			StdOut.println("Smoothing param: " + scale + " Accuracy: " + (correctNum + 0.0)/testExNum);
		}
	}

	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		if(args[2].equals("stream")) {
			streamTest(args[0], args[1]);
		} else {
			test(args[0], args[1]);
		}
		StdOut.println(sw.elapsedTime());
	}
}