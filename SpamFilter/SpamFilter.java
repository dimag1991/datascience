import java.util.HashSet;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.*;

class SpamFilter {

	private HashSet<String> vocabulary = new HashSet<String>();
	private HashMap<String, Double> priors = new HashMap<String, Double>();
	private HashMap<String, HashMap<String, Double>> naiveBayesParams = new HashMap<>();
	private int virtualExamplesScale;
	private int virtualExamplesNum;
	
	public SpamFilter(HashMap<String, List<String>> data, int virtualExamplesScale) {
		this.virtualExamplesScale = virtualExamplesScale;
		createVocabulary(data);
		virtualExamplesNum = virtualExamplesScale*vocabulary.size();
		train(data);
	}
	
	public SpamFilter(String dataFile, int virtualExamplesScale) throws IOException {
		this.virtualExamplesScale = virtualExamplesScale;
		BufferedReader input =  new BufferedReader(new FileReader(dataFile));
		int spamCounts = 0;
		int hamCounts = 0;
		int trainExNum = 0;
		long totalSpamWordsNum = 0;
		long totalHamWordsNum = 0;
		HashMap<String, HashMap<String, Integer>> wordCounts = new HashMap<>();
        try {
            String line = null;
            while ((line = input.readLine()) != null) {
				trainExNum++;
				String[] tokens = line.split(" ");
				String classLabel = tokens[1];
                if(classLabel.equals("spam")) {
					spamCounts++;
				} else {
					hamCounts++;
				}
				for(int i = 2; i < tokens.length; i += 2) {
					vocabulary.add(tokens[i]);
					int count = Integer.parseInt(tokens[i+1]);
					if(classLabel.equals("spam")) {
						totalSpamWordsNum += count;
					} else {
						totalHamWordsNum += count;
					}
					if(wordCounts.containsKey(classLabel)) {
						if(wordCounts.get(classLabel).containsKey(tokens[i])) {
							wordCounts.get(classLabel).put(tokens[i], wordCounts.get(classLabel).get(tokens[i]) + count);
						} else {
							wordCounts.get(classLabel).put(tokens[i], count);
						}
					} else {
						wordCounts.put(classLabel, new HashMap<String, Integer>());
						wordCounts.get(classLabel).put(tokens[i], count);
					}
				}
            }
        }
        finally {
            input.close();
        }
		
		virtualExamplesNum = virtualExamplesScale*vocabulary.size();
		
		priors.put("spam", (spamCounts + 0.0)/trainExNum);
		priors.put("ham", (hamCounts + 0.0)/trainExNum);
		
		for(String label : wordCounts.keySet()) {
			for(String word : vocabulary) {
				int wordInClassOccurNum = 0;
				if(wordCounts.get(label).containsKey(word)) {
					wordInClassOccurNum = wordCounts.get(label).get(word);
				}
				double prob;
				if(label.equals("spam")) {
					prob = (wordInClassOccurNum + virtualExamplesScale + 0.0)/(totalSpamWordsNum + virtualExamplesNum);
				} else {
					prob = (wordInClassOccurNum + virtualExamplesScale + 0.0)/(totalHamWordsNum + virtualExamplesNum);
				}
				if(naiveBayesParams.containsKey(label)) {
					naiveBayesParams.get(label).put(word, prob);
				} else {
					naiveBayesParams.put(label, new HashMap<String, Double>());
					naiveBayesParams.get(label).put(word, prob);
				}
			}
		}
	}
	
	private void createVocabulary(HashMap<String, List<String>> data) {
		for(String label : data.keySet()) {
			for(String email : data.get(label)) {
				String[] tokens = email.split(" ");
				for(int i = 2; i < tokens.length; i += 2) {
					vocabulary.add(tokens[i]);
				}
			}	
		}
	}
	
	private void train(HashMap<String, List<String>> data) {
		int corpusSize = 0;
		for(String label : data.keySet()) {
			corpusSize += data.get(label).size();
		}
		for(String label : data.keySet()) {
			priors.put(label, (data.get(label).size() + 0.0)/corpusSize);
			long totalInClassWordsNum = 0;
			HashMap<String, Integer> wordCounts = new HashMap<String, Integer>();
			for(String email : data.get(label)) {
				String[] tokens = email.split(" ");
				for(int i = 3; i < tokens.length; i += 2) {
					String word = tokens[i-1];
					int counts = Integer.parseInt(tokens[i]);
					totalInClassWordsNum += counts;
					if(wordCounts.containsKey(word)) {
						wordCounts.put(word, wordCounts.get(word) + counts);
					} else {
						wordCounts.put(word, counts);
					}
				}
			}
			for(String word : vocabulary) {
				int wordInClassOccurNum = 0;
				if(wordCounts.containsKey(word)) {
					wordInClassOccurNum = wordCounts.get(word);
				}
				double prob =  (wordInClassOccurNum + virtualExamplesScale + 0.0)/(totalInClassWordsNum + virtualExamplesNum);
				if(naiveBayesParams.containsKey(label)) {
					naiveBayesParams.get(label).put(word, prob);
				} else {
					naiveBayesParams.put(label, new HashMap<String, Double>());
					naiveBayesParams.get(label).put(word, prob);
				}
			}
		}
	}
	
	public String getClassLabel(String[] email) {
		String classLabel = null;
		double posteriorProb = Double.NEGATIVE_INFINITY;
		for(String label : priors.keySet()) {
			double currProb = Math.log(priors.get(label));
			for(int i = 2; i <  email.length; i += 2) {
				currProb += Integer.parseInt(email[i+1])*Math.log(naiveBayesParams.get(label).get(email[i]));
			}
			if(posteriorProb < currProb) {
				posteriorProb = currProb;
				classLabel = label;
			}
		}
		return classLabel;
	}
}