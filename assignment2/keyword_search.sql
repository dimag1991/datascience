CREATE VIEW [CORPUS] AS
	SELECT * FROM frequency
		UNION
	SELECT 'q' as docid, 'washington' as term, 1 as count 
		UNION
	SELECT 'q' as docid, 'taxes' as term, 1 as count
		UNION 
	SELECT 'q' as docid, 'treasury' as term, 1 as count;

SELECT A.docid as d1, SUM(A.count * B.count) as Similarity
	FROM [CORPUS] A, [CORPUS] B
WHERE A.term = B.term AND B.docid = 'q'
GROUP BY A.docid, B.docid
ORDER BY Similarity desc;