SELECT d1, d2, Similarity
	FROM (
		SELECT A.docid as d1, B.docid as d2, SUM(A.count * B.count) as Similarity
			FROM frequency A, frequency B
		WHERE A.term = B.term AND A.docid < B.docid
		GROUP BY A.docid, B.docid
	)
WHERE d1 = '10080_txt_crude' AND d2 = '17035_txt_earn';