class PageViews extends TrainingExample {
	public PageViews(int[] data, int isViewedAnotherPage) {
		attributes = data;
		label = isViewedAnotherPage;
	}
	
	public String toString() {
		String s = "";
		for(int i = 0; i < attributes.length; i++) {
			s += attributes[i] + " ";
		}
		s += label;
		return s;
	}
	
	/* public static void main(String[] args) {
		int[] data = {1, 32, 33, 1, 2, 3, 4};
		TrainingExample p = new PageViews(data, 0);
		StdOut.println(p);
	}  */
}