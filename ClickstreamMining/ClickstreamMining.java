import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.io.*;

class ClickstreamMining {
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		String dataFile = args[0];
		String labelsFile = args[1];
		List<String> dataLines = new ArrayList<String>();
		List<Integer> labels = new ArrayList<Integer>();
		BufferedReader inputData =  new BufferedReader(new FileReader(dataFile));
		BufferedReader inputLabels =  new BufferedReader(new FileReader(labelsFile));
        try {
            String line = null;
            while (( line = inputData.readLine()) != null) {
				dataLines.add(line);
				labels.add(Integer.parseInt(inputLabels.readLine().substring(0, 1)));
            }
        }
        finally {
            inputData.close();
			inputLabels.close();
        } 
		int attributesNum = dataLines.get(0).split(" ").length;
		int[] branchingFactors = new int[attributesNum];
		int trainingExNum = labels.size();		
		List<TrainingExample> data = new ArrayList<TrainingExample>();	
		Set<Integer>[] sets = new HashSet[attributesNum];
		for(int i = 0; i < attributesNum; i++) {
			sets[i] = new HashSet<Integer>();
		}
		for(int i = 0; i < trainingExNum; i++) {
			int[] attributes = new int[attributesNum];
			String[] trainingExample = dataLines.get(i).split(" ");
			for(int j = 0; j < attributesNum; j++) {
				attributes[j] = Integer.parseInt(trainingExample[j]);
				sets[j].add(attributes[j]);
			}
			TrainingExample views = new PageViews(attributes, labels.get(i));
			data.add(views);
		}
		for(int i = 0; i < attributesNum; i++) {
			branchingFactors[i] = sets[i].size();
		}	
		DecisionTree tree = new DecisionTree(data, branchingFactors, 30);
		
		
		String testDataFile = args[2];
		String testLabelsFile = args[3];
		BufferedReader inputTestData =  new BufferedReader(new FileReader(testDataFile));
		BufferedReader inputTestLabels =  new BufferedReader(new FileReader(testLabelsFile));
		int totalTest = 0, correctTest = 0;
        try {
            String line = null;
            while (( line = inputTestData.readLine()) != null) {
				totalTest++;
				int[] attributes = new int[attributesNum];
				String[] features = line.split(" ");
				for(int i = 0; i < attributesNum; i++) {
					attributes[i] = Integer.parseInt(features[i]);
				}
				int label = tree.classify(attributes);
				if(label == Integer.parseInt(inputTestLabels.readLine().substring(0, 1))) {
					correctTest++;
				}
            }
        }
        finally {
            inputTestData.close();
			inputTestLabels.close();
        } 
		StdOut.println("\nAccuracy: " + (correctTest + 0.0)/totalTest);
		StdOut.println("\nTiming results: " + sw.elapsedTime());
	}
}