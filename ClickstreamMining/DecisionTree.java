import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

class DecisionTree {

	private Node root;
	private int[] branchingFactors;
	private int attributesNumber;
	private int maxDepth;

	private class Node {
		private Integer attribute;
		private Integer classLabel;
		private boolean isLeaf;
		private Node[] branches;
	}

	public DecisionTree(List<TrainingExample> data, int[] branchingFactors, int maxDepth) {
		this.maxDepth = maxDepth;
		this.branchingFactors = branchingFactors;
		attributesNumber = branchingFactors.length;
		HashSet<Integer> attributes = new HashSet<Integer>();
		for(int i = 0; i < attributesNumber; i++) {
			attributes.add(i);
		}
		root = ID3(data, attributes, 0);
	}
	
	private Node ID3(List<TrainingExample> data, HashSet<Integer> attributes, int depth) {
		
		StdOut.print(depth + " ");
		
		Node x = new Node();
		int posNumber = 0, negNumber = 0, size = data.size();
		for(TrainingExample example : data) {
			if(example.getClassLabel() == 1) {
				posNumber++;
			} else {
				negNumber++;
			}
		}
		if(posNumber > negNumber) {
			x.classLabel = 1;
		} else {
			x.classLabel = 0;
		}
		if(posNumber == 0 || negNumber == 0 || attributes.isEmpty()  /* || depth == maxDepth */) {
			x.isLeaf = true;
			return x;
		}
		int bestAttribute = -1;
		List<TrainingExample>[] bestPartition = null;
		double bestGain = Double.NEGATIVE_INFINITY;
		double entropy = calcEntropy(posNumber, negNumber, size);
		for(int attributeNum : attributes) {
			List<TrainingExample>[] partition = (ArrayList<TrainingExample>[]) (new ArrayList[branchingFactors[attributeNum]]);
			for(int i = 0; i < branchingFactors[attributeNum]; i++) {
				partition[i] = new ArrayList<TrainingExample>();
			}
			for(TrainingExample example : data) {				
				partition[example.getAttributeValue(attributeNum)-1].add(example);
			}
			double currGain = entropy;
			for(int i = 0; i < branchingFactors[attributeNum]; i++) {
				int currPosNum = 0, currNegNum = 0, currSize = partition[i].size();
				for(TrainingExample example : partition[i]) {
					if(example.getClassLabel() == 0) {
						currNegNum++;
					} else {
						currPosNum++;
					}
				}
				if(currSize > 0) {
					currGain -= (currSize + 0.0)/size*calcEntropy(currPosNum, currNegNum, currSize);
				}
			}
			if(currGain > bestGain) {
				bestGain = currGain;
				bestPartition = partition;
				bestAttribute = attributeNum;
			}
		}
		
		if(bestGain == 0.0) {
			x.isLeaf = true;
			return x;
		}
		
		data = null;
		x.attribute = bestAttribute;
		
		x.branches = new Node[branchingFactors[bestAttribute]];
		int branchNum = x.branches.length;
		for(int i = 0; i < branchNum; i++) {
			int currSize = bestPartition[i].size();
			if(currSize == 0) {
				Node leaf = new Node();
				leaf.isLeaf = true;
				int pos = 0, neg = 0;
				for(TrainingExample ex : bestPartition[i]) {
					if(ex.getClassLabel() == 1) {
						pos++;
					} else {
						neg++;
					}
				}
				leaf.classLabel = (pos > neg) ? 1 : 0;
				x.branches[i] = leaf;
 			} else {
				attributes.remove(bestAttribute);
				x.branches[i] = ID3(bestPartition[i], attributes, depth + 1);
				attributes.add(bestAttribute);
			}
		}
		return x;
	}
	
	public int classify(int[] attributes) {
		return classify(root, attributes);
	}
	
	private int classify(Node x, int[] attributes) {
		if(x.isLeaf) {
			return x.classLabel;
		}
		return classify(x.branches[attributes[x.attribute]-1], attributes);
    }
	
	private double calcEntropy(int posNumber, int negNumber, int size) {
		double posFrac = (posNumber + 0.0)/size;
		double negFrac = (negNumber + 0.0)/size;
		return -posFrac*(Math.log(posFrac)/Math.log(2)) - negFrac*(Math.log(negFrac)/Math.log(2));
	}
}