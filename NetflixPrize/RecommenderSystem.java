import java.io.*;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

class RecommenderSystem {

	private int usersNumber;
	private int trainingRatingsNumber;
	private double pearsonTiming;
	private double predictionTiming;
	private Stopwatch sw = new Stopwatch();
	private HashMap<Integer, User> users = new HashMap<Integer, User>();

	public RecommenderSystem(String trainingData) throws IOException {
		setUserData(trainingData);
	}
	
	public void setUserData(String dataFile) throws IOException {
		HashMap<Integer, List<String>> data = new HashMap<Integer, List<String>>();
        BufferedReader input =  new BufferedReader(new FileReader(dataFile));
        try {
            String line = null;
            while (( line = input.readLine()) != null) {
				trainingRatingsNumber++;
				String[] tokens = line.split(",");
				int userId = Integer.parseInt(tokens[1]);
                if(data.containsKey(userId)) {
					data.get(userId).add(line);
				} else {
					data.put(userId, new ArrayList<String>());
					data.get(userId).add(line);
				}
            }
        }
        finally {
            input.close();
        }
		usersNumber = data.size();
		for(Integer userId : data.keySet()) {
			users.put(userId, new User(userId, data.get(userId)));
		}
	}
	
	public double predictRating(int activeId, int movieId) {
		
		double predictionStart = sw.elapsedTime();
		
		double rating = 0;
		double normalizationCoeff = 0;
		User active = users.get(activeId);
		User currPassive = null;
		for(int id : users.keySet()) {
			currPassive = users.get(id);
			if(currPassive.isRated(movieId)) {
				double start = sw.elapsedTime();
				double w = active.PearsonCorrelation(currPassive);			
				pearsonTiming = pearsonTiming + (sw.elapsedTime() - start);
				rating += w*(currPassive.getMovieRating(movieId) - currPassive.getAverageRating());
				normalizationCoeff += Math.abs(w);
			}
		}
		
		predictionTiming = predictionTiming + (sw.elapsedTime() - predictionStart);
		
		if(rating == 0 && normalizationCoeff == 0) {
			return active.getAverageRating();
		} else {
			rating = rating/normalizationCoeff + active.getAverageRating();
			return rating;
		}
	}

	public void printDataSummary() {
		StdOut.println("\n\n*** Training data summary: ***\n");
		StdOut.println("Number of users to collaborate: " + usersNumber);
		StdOut.println("Number of ratings in training set: " + trainingRatingsNumber);
		StdOut.println("Pearson timing: " + pearsonTiming);
		StdOut.println("Prediction timing: " + predictionTiming);
	}
	
	public static void main(String[] args) throws IOException {
		Stopwatch sw = new Stopwatch();
		RecommenderSystem rs = new RecommenderSystem(args[0]);
		BufferedReader input =  new BufferedReader(new FileReader(args[1]));
		int count = 0;
		double MAE = 0;
		double RMSD = 0;
        try {
            String line = null;
            while (( line = input.readLine()) != null) {
				count++;
				String[] tokens = line.split(",");
				int movie = Integer.parseInt(tokens[0]);
				int user = Integer.parseInt(tokens[1]);
				double actualRating = Double.parseDouble(tokens[2]);
				double predictedRating = rs.predictRating(user, movie);
				MAE += Math.abs(actualRating  - predictedRating);
				RMSD += (actualRating  - predictedRating)*(actualRating  - predictedRating);
				if(count%100 == 0) {
					StdOut.println(count);
				}
				//if(count == 400) break;
			}
		} finally {
            input.close();
        }
		MAE = MAE/count;
		RMSD = Math.sqrt(RMSD/count);
		StdOut.println("MAE = " + MAE);
		StdOut.println("RMSD = " + RMSD); 
		rs.printDataSummary();
		//User.printCacheStats();
		StdOut.println("\n\n*** Timing results: *** \n\nElapsed time: " + sw.elapsedTime());
	}
}