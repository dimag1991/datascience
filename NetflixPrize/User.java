import java.util.HashMap;
import java.util.List;

class User {

	private int id; 
	private double averageRating;
	private HashMap<Integer, Double> ratings  = new HashMap<Integer, Double>();
	private static HashMap<Integer, HashMap<Integer, Double>> pearsonCache = new HashMap<Integer, HashMap<Integer, Double>>();
	private static double cacheHits;
	private static int total;
	
	public User(int id, List<String> userData) {
		this.id = id;
		double acc = 0.0;
		for(String trainingExample : userData) {
			String[] tokens = trainingExample.split(",");
			int movieId = Integer.parseInt(tokens[0]);
			double rating = Double.parseDouble(tokens[2]);
			acc += rating;
			ratings.put(movieId, rating);
		}
		averageRating = acc/userData.size();
	}
	
	public int getId() {
		return id;
	}
	
	public double getAverageRating() {
		return averageRating;
	}
	
	public boolean isRated(int movieId) {
		return ratings.containsKey(movieId);
	}
	
	public double getMovieRating(int movieId) {
		return ratings.get(movieId);
	}
	
	public HashMap<Integer, Double> getRatings() {
		return ratings;
	}
	
	public double PearsonCorrelation(User that) {
	
		/* if(pearsonCache.containsKey(this.id) && pearsonCache.get(this.id).containsKey(that.id)) {
			//StdOut.println("From cache");
			cacheHits++;
			return pearsonCache.get(this.id).get(that.id);
		} */
		//total++;
		double activeUserDiff = 0, passiveUserDiff = 0;
		double crossSum = 0, activeSqSum = 0, passiveSqSum = 0;
		for(int movie : ratings.keySet()) {
			if(that.isRated(movie)) {
				activeUserDiff = ratings.get(movie) - averageRating; 
				passiveUserDiff = that.ratings.get(movie) - that.averageRating;
				crossSum += activeUserDiff*passiveUserDiff;
				activeSqSum += activeUserDiff*activeUserDiff;
				passiveSqSum += passiveUserDiff*passiveUserDiff;
			}
		}
		Double w = crossSum/Math.sqrt(activeSqSum*passiveSqSum);
		if(w.equals(Double.NaN)) {
			w = 0.0;
		} 
		/* if(pearsonCache.containsKey(this.id)) {
			pearsonCache.get(this.id).put(that.id, w);
		} else {
			pearsonCache.put(this.id, new HashMap<Integer, Double>());
			pearsonCache.get(this.id).put(that.id, w);
		}
		if(pearsonCache.containsKey(that.id)) {
			pearsonCache.get(that.id).put(this.id, w);
		} else {
			pearsonCache.put(that.id, new HashMap<Integer, Double>());
			pearsonCache.get(that.id).put(this.id, w);
		}  */
		return w;	
	}
	
	public static void printCacheStats() {
		StdOut.println("\n\n*** Cache hit rate: ***\n\n" + cacheHits/total);
	}
}