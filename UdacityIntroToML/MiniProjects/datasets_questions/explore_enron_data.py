#!/usr/bin/python

""" 
    starter code for exploring the Enron dataset (emails + finances) 
    loads up the dataset (pickled dict of dicts)

    the dataset has the form
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person
    you should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))

#print sum([1 for person in enron_data if enron_data[person]['poi']])
#print enron_data['COLWELL WESLEY']['from_this_person_to_poi']
#print enron_data['SKILLING JEFFREY K']['exercised_stock_options']

#print sum([1 for person in enron_data if enron_data[person]['salary'] != 'NaN'])

#print sum([1 for person in enron_data if enron_data[person]['email_address'] != 'NaN'])
print sum([1 for person in enron_data if enron_data[person]['total_payments'] == 'NaN'])/(len(enron_data) + 0.0)

#print sum([1 for person in enron_data if enron_data[person]['total_payments'] == 'NaN' and enron_data[person]['poi']])/(len(enron_data) + 0.0)
