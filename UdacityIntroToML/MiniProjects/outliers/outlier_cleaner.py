#!/usr/bin/python

def outlierCleaner(predictions, ages, net_worths):
    """
        clean away the 10% of points that have the largest
        residual errors (different between the prediction
        and the actual net worth)

        return a list of tuples named cleaned_data where 
        each tuple is of the form (age, net_worth, error)
    """
    residuals = [(abs(predictions[i] - net_worths[i]), ages[i], net_worths[i]) for i in range(len(ages))]  
    residuals.sort()
    
    cleaned_data = [(residuals[i][1], residuals[i][2], residuals[i][0]) for i in range(int(len(ages)*0.9))]
    
    return cleaned_data

