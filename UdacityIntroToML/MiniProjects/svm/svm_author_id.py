#!/usr/bin/python

""" 
    this is the code to accompany the Lesson 2 (SVM) mini-project

    use an SVM to identify emails from the Enron corpus by their authors
    
    Sara has label 0
    Chris has label 1

"""
    
import sys
from time import time
sys.path.append("../tools/")
from email_preprocess import preprocess
from sklearn import svm

### features_train and features_test are the features for the training
### and testing datasets, respectively
### labels_train and labels_test are the corresponding item labels
features_train, features_test, labels_train, labels_test = preprocess()



#features_train = features_train[:len(features_train)/100] 
#labels_train = labels_train[:len(labels_train)/100] 

#########################################################
### your code goes here ###

clf = svm.SVC(C=10000, kernel='rbf')
clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

accuracy = sum([1 for i in range(len(pred)) if pred[i] == labels_test[i]])/(len(pred)+0.0)

print accuracy

chris_num = sum([1 for i in pred if i == 1])

print chris_num

#########################################################


