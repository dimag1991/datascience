library(caret)
library(rpart)
library(randomForest)
library(e1071)

optical_properties <- read.csv(file="seaflow_21min.csv",head=TRUE,sep=",")

print("To answer questions 1 and 2")
summary(optical_properties)

trainIndex <- createDataPartition(optical_properties$pop, p = 0.5, list = FALSE, times = 1)
optical_properties_train <- optical_properties[ trainIndex,]
optical_properties_test  <- optical_properties[-trainIndex,]

print("To answer questions 3")
mean(optical_properties_train$time)

a <- ggplot(data=optical_properties, aes(x = pe, y = chl_small, color = pop))
a <- a + geom_point()
a

fol <- formula(pop ~ fsc_small + fsc_perp + fsc_big + pe + chl_big + chl_small)

model1 <- rpart(fol, method="class", data=optical_properties_train)
print("To answer questions 5, 6, 7")
print(model1)
p1 <- predict(model1, newdata = optical_properties_test, type = "class")
print("To answer questions 8")
sum(p1 == optical_properties_test$pop)/length(optical_properties_test$pop)

model2 <- randomForest(fol, data=optical_properties_train)
p2 <- predict(model2, newdata = optical_properties_test, type = "class")
print("To answer questions 9")
sum(p2 == optical_properties_test$pop)/length(optical_properties_test$pop)
print("To answer questions 10")
importance(model2)

model3 <- svm(fol, data=optical_properties_train)
p3 <- predict(model3, newdata = optical_properties_test, type = "class")
print("To answer questions 11")
sum(p3 == optical_properties_test$pop)/length(optical_properties_test$pop)

print("To answer questions 12")
table(pred=p1, true = optical_properties_test$pop)
table(pred=p2, true = optical_properties_test$pop)
table(pred=p3, true = optical_properties_test$pop)

print("To answer questions 13")
max(optical_properties$pe) - min(optical_properties$pe)
max(optical_properties$fsc_small) - min(optical_properties$fsc_small)
max(optical_properties$fsc_big) - min(optical_properties$fsc_big)
max(optical_properties$chl_big) - min(optical_properties$chl_big)
max(optical_properties$chl_small) - min(optical_properties$chl_small)
max(optical_properties$fsc_perp) - min(optical_properties$fsc_perp)

a <- ggplot(data=optical_properties, aes(x = chl_big, y = time, color = (file_id == 208)))
a <- a + geom_point()
a

filtered_optical_properties <- optical_properties[optical_properties$file_id != 208, ]
filtered_trainIndex <- createDataPartition(filtered_optical_properties$pop, p = 0.5, list = FALSE, times = 1)
filtered_optical_properties_train <- filtered_optical_properties[ trainIndex,]
filtered_optical_properties_test  <- filtered_optical_properties[-trainIndex,]

model4 <- svm(fol, data=filtered_optical_properties_train)
p4 <- predict(model4, newdata = filtered_optical_properties_test, type = "class")
print("To answer questions 14")
sum(p4 == filtered_optical_properties_test$pop)/length(filtered_optical_properties_test$pop)
 