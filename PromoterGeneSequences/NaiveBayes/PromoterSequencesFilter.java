import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;

class PromoterSequencesFilter {
	
	private int attributeValuesNum = 4;
	private int attributesNum = 57;
	private int classNum = 2;
	private int virtualExamplesNum;
	private double[] priors;
	private double[][][] params;
	
	public PromoterSequencesFilter(List<int[]>[] data, int virtualExamplesNum) {
		this.virtualExamplesNum = virtualExamplesNum;
		int totalSize = data[0].size() + data[1].size();
		int promoterSize = data[1].size();
		int nonPromoterSize = data[0].size();
		priors = new double[classNum];
		priors[0] = (nonPromoterSize + 0.0)/totalSize;
		priors[1] = (promoterSize + 0.0)/totalSize;
		params = new double[classNum][attributeValuesNum][attributesNum];
		for(int i = 0; i < classNum; i++) {
			int[][] counts = new int[attributeValuesNum][attributesNum];
			for(int[] ex : data[i]) {
				for(int j = 0; j < ex.length; j++) {
					counts[ex[j]][j]++;
				}
			}
			for(int j = 0; j < attributeValuesNum; j++) {
				for(int k = 0; k < attributesNum; k++) {
					params[i][j][k] = (counts[j][k] + (virtualExamplesNum + 0.0)/attributeValuesNum)/(data[i].size() + virtualExamplesNum);
				}
			}
		}
	}
	
	public int classify(int[] data) {
		double posProb = Math.log(priors[1]);
		double negProb = Math.log(priors[0]);
		for(int i = 0; i < data.length; i++) {
			posProb += Math.log(params[1][data[i]][i]);
			negProb += Math.log(params[0][data[i]][i]);
		}
		return posProb > negProb ? 1 : 0;
	}
	
	public static void main(String[] args) throws IOException {
		HashMap<String, Integer> attribMap = new HashMap<>();
		attribMap.put("a", 0);
		attribMap.put("c", 1);
		attribMap.put("g", 2);
		attribMap.put("t", 3);
		int attributesNum = 57;
		int classLabelsNum = 2;
		List<int[]>[] data = (ArrayList<int[]>[]) (new ArrayList[classLabelsNum]);
		for(int i = 0; i < classLabelsNum; i++) {
			data[i] = new ArrayList<int[]>();
		}
		BufferedReader input =  new BufferedReader(new FileReader(args[0]));
        try {
            String line = null;
            while (( line = input.readLine()) != null) {
				String[] tokens = line.split(",");
				int classLabel = tokens[0].equals("+") ? 1 : 0;
				int[] attributes = new int[attributesNum];
				for(int i = 0; i < attributesNum; i++) {
					attributes[i] = attribMap.get(tokens[i+1]);
				}
				data[classLabel].add(attributes);
             }
        }
        finally {
            input.close();
        } 
		int virtualExNum = 4;
		PromoterSequencesFilter pf = new PromoterSequencesFilter(data, virtualExNum);
		int total = 0, correct = 0;
		input =  new BufferedReader(new FileReader(args[1]));
		 try {
            String line = null;
            while (( line = input.readLine()) != null) {
				total++;
				String[] tokens = line.split(",");
				int classLabel = tokens[0].equals("+") ? 1 : 0;
				int[] attributes = new int[attributesNum];
				for(int i = 0; i < attributesNum; i++) {
					attributes[i] = attribMap.get(tokens[i+1]);
				}
				int prediction = pf.classify(attributes);
				if(classLabel == prediction) {
					correct++;
				}
             }
        }
        finally {
            input.close();
        } 
		StdOut.println("Accuracy: " + (correct + 0.0)/total);
	}
}