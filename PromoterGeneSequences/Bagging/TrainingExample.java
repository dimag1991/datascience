abstract class TrainingExample {
	protected int[] attributes;
	protected int label;
	
	public int getAttributeValue(int i) {
		return attributes[i];
	}
	
	public int getLength() {
		return attributes.length;
	}
	
	public int getClassLabel() {
		return label;
	}
}