import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;

class PromoterGeneSequences {
	public static void main(String[] args) throws IOException {
		List<String> lines = new ArrayList<String>();
		BufferedReader input =  new BufferedReader(new FileReader(args[0]));
        try {
            String line = null;
            while (( line = input.readLine()) != null) {
				lines.add(line);
            }
        }
        finally {
            input.close();
        }
		HashMap<String, Integer> attribMap = new HashMap<>();
		attribMap.put("a", 1);
		attribMap.put("c", 2);
		attribMap.put("g", 3);
		attribMap.put("t", 4);
		int attributesNum = 57;
		List<TrainingExample> data = new ArrayList<TrainingExample>();
		for(String line : lines) {
			String[] tokens = line.split(",");
			int[] attributes = new int[attributesNum];
			for(int i = 0; i < attributesNum; i++) {
				attributes[i] = attribMap.get(tokens[i+1]);
			}
			int isPromoter = (tokens[0].equals("+")) ? 1 : 0;
			TrainingExample ex = new DNASequence(attributes, isPromoter);
			data.add(ex);
		}
		int[] branchFactors = new int[attributesNum];
		for(int i = 0; i < attributesNum; i++) {
			branchFactors[i] = attribMap.size();
		}
		List<Integer> testLabels = new ArrayList<Integer>();
		List<int[]> testData = new ArrayList<int[]>();
		input =  new BufferedReader(new FileReader(args[1]));
		try {
			String line = null;
			while (( line = input.readLine()) != null) {
				String[] tokens = line.split(",");
				int[] attributes = new int[attributesNum];
				for(int i = 0; i < attributesNum; i++) {
					attributes[i] = attribMap.get(tokens[i+1]);
				}
				testData.add(attributes);
				testLabels.add(tokens[0].equals("+") ? 1 : 0);
			}
		}
		finally {
			input.close();
		}
		int samplingNumber =  Integer.parseInt(args[2]);
		int N = Integer.parseInt(args[3]), total = 0, correct = 0, size = testData.size();
		for(int k = 0; k < N; k++) {
			RandomForest forest = new RandomForest(data, branchFactors, samplingNumber, 3);
			for(int j = 0; j < size; j++) {
				total++;
				int prediction = forest.classify(testData.get(j));
				int label = testLabels.get(j);
				if(prediction == label) {
					correct++;
				}
			}
		}
		StdOut.println("\nAccuracy: " + (correct + 0.0)/total);
	}
}