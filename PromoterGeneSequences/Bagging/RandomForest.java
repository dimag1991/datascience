import java.util.List;
import java.util.ArrayList;

class RandomForest {
	
	private DecisionTree[] forest;
	private int samplingNumber;
	
	public RandomForest(List<TrainingExample> data, int[] branchingFactors, int samplingNumber, int maxDepth) {
		this.samplingNumber = samplingNumber;
		forest = new DecisionTree[samplingNumber];
		for(int i = 0; i < samplingNumber; i++) {
			List<TrainingExample> bootstrapReplica = generateBootstrapReplica(data);
			forest[i] = new DecisionTree(bootstrapReplica, branchingFactors, maxDepth);
		}
	}
	
	private List<TrainingExample> generateBootstrapReplica(List<TrainingExample> data) {
		int sampleSize = data.size();
		List<TrainingExample> sample = new ArrayList<TrainingExample>();
		for(int i = 0; i < sampleSize; i++) {
			sample.add(data.get(StdRandom.uniform(sampleSize)));
		}
		return sample;
	}
	
	public int classify(int[] attributes) { 
		int pos = 0, neg = 0;
		for(int i = 0; i < samplingNumber; i++) {
			int label = forest[i].classify(attributes);
			if(label == 1) {
				pos++;
			} else {
				neg++;
			}			
		}
		return (pos > neg) ? 1 : 0;
	}
}